﻿// moving Header
$("#mainNav")
    .affix({
        offset: {
            top: 100
        }
    });

// Body under fixed header with dynamic height
var divHeight = $("#mainNav").height();
$("#mainBody").css("margin-top", divHeight + "px");

$(window)
    .resize(function() {
        const divHeight = $("#mainNav").height();
        $("#mainBody").css("margin-top", divHeight + "px");
    });

$(function() {
    $("#btn-get-table")
        .click(function() {
            $.ajax({
                    type: "GET",
                    url: "/Home/GetTable",
                    beforeSend: function() {
                        $("#ajax-chart")
                            .html("<img alt=\"loading.gif\" src=\"../Content/light_blue_material_design_loading.gif\" />");
                    }
                })
                .success(function(responseData) {
                    $("#ajax-chart").html(responseData);
                });
        });
});

$(function() {
    $("#btn-get-tb-chart")
        .click(function() {
            $.ajax({
                    type: "GET",
                    url: "/Home/GetTbChart",
                    beforeSend: function() {
                        $("#ajax-chart")
                            .html("<img alt=\"loading.gif\" src=\"../Content/light_blue_material_design_loading.gif\" />");
                    }
                })
                .success(function(responseData) {
                    $("#ajax-chart").html(responseData);
                });
        });
});

$(function() {
    $("#btn-get-zapas-prochn-chart")
        .click(function() {
            $.ajax({
                    type: "GET",
                    url: "/Home/GetZapasProchnChart",
                    beforeSend: function() {
                        $("#ajax-chart")
                            .html("<img alt=\"loading.gif\" src=\"../Content/light_blue_material_design_loading.gif\" />");
                    }
                })
                .success(function(responseData) {
                    $("#ajax-chart").html(responseData);
                });
        });
});

$(function () {
    $("#btn-get-operacion-richag-chart")
        .click(function () {
            $.ajax({
                type: "GET",
                url: "/Home/GetOperacionRichagChart",
                beforeSend: function () {
                    $("#ajax-chart")
                        .html("<img alt=\"loading.gif\" src=\"../Content/light_blue_material_design_loading.gif\" />");
                }
            })
                .success(function (responseData) {
                    $("#ajax-chart").html(responseData);
                });
        });
});

var GenerateChar = function(rows, axis) {
    var rowsData = [];
    var rowTypesData = {};

    rows.forEach(function(item, i, arr) {
        var row = [];

        row.push(item.Name);
        rowTypesData[item.Name] = item.Type;

        item.Points.forEach(function(item1, j, arr2) {
            row.push(item1);
        });

        rowsData.push(row);
    });

    var axisData = [];
    axis.Points.forEach(function(item1, j, arr2) {
        axisData.push(item1);
    });

    const chart = c3.generate({
        data: {
            columns: rowsData,
            types: rowTypesData
        },
        axis: {
            x: {
                type: "category",
                categories: axisData
            }
        }
    });
};