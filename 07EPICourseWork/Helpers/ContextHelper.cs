﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _07EPICourseWork.Helpers
{
    public class ContextHelper
    {
        public static object Get(string key)
        {
            return HttpContext.Current.Application[key];
        }

        public static void Set(object model, string key)
        {
            HttpContext.Current.Application[key] = model;
        }
    }
}