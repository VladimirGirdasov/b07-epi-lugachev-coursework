﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using _07EPICourseWork.Helpers;
using _07EPICourseWork.Models;


namespace _07EPICourseWork.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var vm = new MainVM
            {
                Input = new Input
                {
                    FC = 137,
                    AVC = 22,
                    MR = 29,
                    Qmin = 1,
                    Qmax = 35
                }
            };

            vm = CalcTable(vm);

            vm.Chart.Invalid = true;

            return View(vm);
        }

        public ActionResult GetTbChart()
        {
            var model = (MainVM)ContextHelper.Get(Resource.CacheMainVM);

            model.Chart.AxisX = new ChartRow();
            model.Chart.Rows = new List<ChartRow>();

            var FCrow = new ChartRow
            {
                Name = "Постоянные затраты (FC) руб.",
                Type = "line"
            };
            var TCrow = new ChartRow
            {
                Name = "Общие затраты (TC) руб.",
                Type = "line"
            };
            var TVCrow = new ChartRow
            {
                Name = "Переменные затраты (TVC) руб.",
                Type = "line"
            };
            var TRrow = new ChartRow
            {
                Name = "Доход (TR) руб.",
                Type = "line"
            };
            var Axis = new ChartRow
            {
                Name = "Q"
            };

            foreach (var row in model.Chart.Table)
            {
                FCrow.Points.Add(row.FC);
                TCrow.Points.Add(row.TC);
                TVCrow.Points.Add(row.TVC);
                TRrow.Points.Add(row.TR);
                Axis.Points.Add(row.Q);
            }

            model.Chart.Rows.Add(FCrow);
            model.Chart.Rows.Add(TVCrow);
            model.Chart.Rows.Add(TRrow);
            model.Chart.Rows.Add(TCrow);
            model.Chart.AxisX = Axis;

            model.Chart.TypeOfChart = Resource.TypeTB;

            ContextHelper.Set(model.Chart, Resource.ChartModel);

            Thread.Sleep(250);
            return PartialView("_Chart");
        }

        public ActionResult GetZapasProchnChart()
        {
            var model = (MainVM)ContextHelper.Get(Resource.CacheMainVM);

            model.Chart.AxisX = new ChartRow();
            model.Chart.Rows = new List<ChartRow>();

            var Srow = new ChartRow
            {
                Name = "Запас прочности (S) %",
                Type = "line"
            };
            var Axis = new ChartRow
            {
                Name = "Q"
            };

            foreach (var row in model.Chart.Table)
            {
                Srow.Points.Add(row.S);
                Axis.Points.Add(row.Q);
            }

            model.Chart.Rows.Add(Srow);
            model.Chart.AxisX = Axis;

            model.Chart.TypeOfChart = Resource.TypeZapasPro4n;

            ContextHelper.Set(model.Chart, Resource.ChartModel);

            Thread.Sleep(250);
            return PartialView("_Chart");
        }

        public ActionResult GetOperacionRichagChart()
        {
            var model = (MainVM)ContextHelper.Get(Resource.CacheMainVM);

            model.Chart.AxisX = new ChartRow();
            model.Chart.Rows = new List<ChartRow>();

            var Lrow = new ChartRow
            {
                Name = "Операционный рычаг (L)",
                Type = "area"
            };
            var Axis = new ChartRow
            {
                Name = "Q"
            };

            foreach (var row in model.Chart.Table)
            {
                Lrow.Points.Add(row.L);
                Axis.Points.Add(row.Q);
            }

            model.Chart.Rows.Add(Lrow);
            model.Chart.AxisX = Axis;

            model.Chart.TypeOfChart = Resource.TypeOperRi4ag;

            ContextHelper.Set(model.Chart, Resource.ChartModel);
            SetInputToContext(model);

            Thread.Sleep(250);
            return PartialView("_Chart");
        }

        public ActionResult GetTable()
        {
            var model = (MainVM)ContextHelper.Get(Resource.CacheMainVM);
            
            Thread.Sleep(300);
            return PartialView("_TableChart", model.Chart.Table);
        }

        private void SetInputToContext(MainVM model)
        {
            ContextHelper.Set(model.Input.FC, Resource.LsFC);
            ContextHelper.Set(model.Input.AVC, Resource.LsAVC);
            ContextHelper.Set(model.Input.MR, Resource.LsMR);
            ContextHelper.Set(model.Input.Qmin, Resource.LsQMin);
            ContextHelper.Set(model.Input.Qmax, Resource.LsQMax);
        }

        [HttpPost]
        public ActionResult Index(MainVM model)
        {
            SetInputToContext(model);

            ValidateDatesSequence(model.Input);

            if (ModelState.IsValid)
            {
                model.Chart.Invalid = false;

                model = CalcTable(model);
            }
            else
            {
                model.Chart.Invalid = true;
            }

            return View("Index", model);
        }

        private void ValidateDatesSequence(Input model)
        {
            if (model.Qmax <= model.Qmin)
            {
                ModelState.AddModelError("Qmax", Resource.HomeController_ValidateDatesSequence_Qmin_не_должно_превышать_или_быть_равным_Qmax);
            }
        }

        private MainVM CalcTable(MainVM model)
        {
            var table = new List<Table>();
            var data = model.Input;
            var tr = new Table();

            for (int q = data.Qmin; q <= data.Qmax; q++)
            {
                tr = new Table();

                // Количество
                tr.Q = q;

                // Постоянные затраты
                tr.FC = data.FC;

                // Переменные затраты
                tr.TVC = q*data.AVC;

                // Общие затраты
                tr.TC = tr.FC + tr.TVC;

                // Доход
                tr.TR = q*data.MR;

                // Прибыль
                tr.TP = tr.TR - tr.TC;

                // Операционный рычаг
                if (tr.TP == 0)
                    tr.L = 0;
                else
                tr.L = (tr.TR - tr.TVC)/tr.TP;

                table.Add(tr);
            }

            // Точка безубыточности ШТ
            tr = table.Last();
            if (tr.TR - tr.TVC == 0)
                model.Chart.Tb.TBcount = 0;
            else
                model.Chart.Tb.TBcount = tr.FC /(tr.TR - tr.TVC)* tr.Q;

            // Точка безубыточности РУБ
            if ((tr.TR - tr.TVC) / tr.TR == 0 || tr.TR == 0)
                model.Chart.Tb.TBcurrency = 0;
            else
                model.Chart.Tb.TBcurrency = tr.FC/((tr.TR - tr.TVC)/tr.TR);

            // Запас прочности
            foreach (var t in table)
            {
                if (t.TR == 0)
                    t.S = 0;
                else
                    t.S = (t.TR - model.Chart.Tb.TBcurrency)/t.TR*100;
            }

            model.Chart.Table = table;

            return model;
        }
    }
}