﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _07EPICourseWork.Models
{
    public class ChartRow
    {
        public string Name { get; set; }

        public List<double> Points { get; set; }

        public string Type { get; set; }

        public ChartRow()
        {
            Points = new List<double>();
        }
    }
}