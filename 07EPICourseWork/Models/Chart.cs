﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _07EPICourseWork.Models
{
    public class Chart
    {
        public List<ChartRow> Rows { get; set; }

        public ChartRow AxisX { get; set; }

        public bool Invalid { get; set; }

        public List<Table> Table { get; set; }

        public TB Tb { get; set; }

        public string TypeOfChart { get; set; }

        public Chart()
        {
            Rows = new List<ChartRow>();
            AxisX = new ChartRow();
            Table = new List<Table>();
            Tb = new TB();
        }
    }
}