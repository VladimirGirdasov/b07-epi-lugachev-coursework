﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _07EPICourseWork.Models;

namespace _07EPICourseWork.Models
{
    public class MainVM
    {
        public Input Input { get; set; }

        public Chart Chart { get; set; }

        public MainVM()
        {
            Input = new Input();

            Chart = new Chart();
        }
    }
}