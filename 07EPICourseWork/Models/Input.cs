﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _07EPICourseWork.Models
{
    public class Input
    {
        [DisplayName(@"FC (Постоянные затраты)")]
        [Range(0, int.MaxValue)]
        public double FC { get; set; }

        [DisplayName(@"AVC (Переменные затраты на единицу продукции)")]
        [Range(0, int.MaxValue)]
        public double AVC { get; set; }

        [DisplayName(@"MR (Доход на единицу продукции)")]
        [Range(0, int.MaxValue)]
        public double MR { get; set; }

        [DisplayName(@"Qmin (Количество продукции минимальная граница)")]
        [Range(1,int.MaxValue)]
        public int Qmin { get; set; }

        [DisplayName(@"Qmax (Количество продукции максимальная граница)")]
        [Range(5, int.MaxValue)]
        public int Qmax { get; set; }

        public Input()
        {
            Qmax = 10;
        }
    }
}