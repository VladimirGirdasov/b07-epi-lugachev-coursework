﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace _07EPICourseWork.Models
{
    public class Table
    {
        [DisplayName(@"Кол-во Q шт.")]
        public int Q { get; set; }

        [DisplayName(@"Постоянные затраты (FC) руб.")]
        public double FC { get; set; }

        [DisplayName(@"Переменные затраты (TVC) руб.")]
        public double TVC { get; set; }

        [DisplayName(@"Общие затраты (TC) руб.")]
        public double TC { get; set; }

        [DisplayName(@"Доход (TR) руб.")]
        public double TR { get; set; }

        [DisplayName(@"Прибыль (TP) руб.")]
        public double TP { get; set; }

        [DisplayName(@"Операционный рычаг (L)")]
        public double L { get; set; }

        [DisplayName(@"Запас прочности (S) %")]
        public double S { get; set; }
    }
}